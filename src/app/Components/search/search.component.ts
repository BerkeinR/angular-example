import { Component, OnInit } from '@angular/core';
import {Song} from '../../Model/song';
import {SongError} from '../../Model/song-error';
import {Observable, of, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import {SongService} from '../../Service/song.service';
import {AbstractControl, FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  songs: Observable<Song[]>;
  errors: string[];
  private searchTerms = new Subject<string>();

  searchForm: FormGroup;


  constructor(private songService: SongService, public fb: FormBuilder) {
    this.searchForm = fb.group({
      item: [null]});
  }

  ngOnInit(): void {
    this.errors = [];
    this.songs = this.searchTerms.pipe(
      // debounceTime(300),
      distinctUntilChanged(),
      switchMap(
        term =>
          term ? this.songService.search(term) : of<Song[]>([])
      ));
  }

  get item(): AbstractControl { return this.searchForm.get('item'); }

  search(): void {
    this.searchTerms.next(this.item.value);
  }
}
