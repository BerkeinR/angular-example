import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {Song} from '../../../Model/song';
import {SongService} from '../../../Service/song.service';
import {SongError} from '../../../Model/song-error';

@Component({
  selector: 'app-delete-song',
  templateUrl: './delete-song.component.html',
  styleUrls: ['./delete-song.component.scss']
})
export class DeleteSongComponent implements OnInit {

  @Input()
  song: Song;

  @Output()
  delete = new EventEmitter<Song>();

  @Output()
  error = new EventEmitter<SongError>();

  constructor(private songService: SongService) { }

  ngOnInit(): void {
  }

  deleteSong(): void {
    this.songService.deleteSong(this.song).subscribe(
      res =>  this.delete.emit(this.song),
      err =>  this.error.emit(err)
    );
  }
}
