import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SongService} from '../../../Service/song.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Song} from '../../../Model/song';
import {SongError} from '../../../Model/song-error';

@Component({
  selector: 'app-edit-song',
  templateUrl: './edit-song.component.html',
  styleUrls: ['./edit-song.component.scss']
})
export class EditSongComponent implements OnInit {

  uniqueID: string;
  currentSong: Song;

  editSongForm: FormGroup;
  errors: string[];

  constructor(public fb: FormBuilder, private songService: SongService, private router: Router,
              private route: ActivatedRoute
  ) {
    this.editSongForm = fb.group({
      name: [null, [Validators.required]],
      album: [null, Validators.required],
      artist: [null, Validators.required]
    });
  }

  ngOnInit(): void {
    this.uniqueID = this.route.snapshot.paramMap.get('uniqueID');
    this.errors = [];
    this.getSong(this.uniqueID);
  }

  getSong(uniqueID: string): void{
    this.songService.getSong(uniqueID).subscribe(song => {
      this.currentSong = song;

      this.name.setValue(song.name);
      this.artist.setValue(song.artist);
      this.album.setValue(song.album);
    });

  }


  get name(): AbstractControl { return this.editSongForm.get('name'); }
  get album(): AbstractControl { return this.editSongForm.get('album'); }
  get artist(): AbstractControl { return this.editSongForm.get('artist'); }


  editSong(): void {
    this.currentSong = {
      uniqueID: this.currentSong.uniqueID,
      name: this.name.value,
      album: this.album.value,
      artist: this.artist.value
    };

    if (this.editSongForm.valid){
      this.songService.updateSong(this.currentSong).subscribe(
        res => this.router.navigate(['/home']).then(),
        err => {
          const songError: SongError = err.error;
          this.errors = songError.error;
        }
      );
    }

  }
}
