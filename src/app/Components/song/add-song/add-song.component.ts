import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Song} from '../../../Model/song';
import {SongService} from '../../../Service/song.service';
import {Router} from '@angular/router';
import {SongError} from '../../../Model/song-error';

@Component({
  selector: 'app-add-song',
  templateUrl: './add-song.component.html',
  styleUrls: ['./add-song.component.scss']
})
export class AddSongComponent implements OnInit {

  addSongForm: FormGroup;
  errors: string[];

  constructor(public fb: FormBuilder, private songService: SongService, private router: Router) {
    this.addSongForm = fb.group({
      name: [null, [Validators.required]],
      album: [null, Validators.required],
      artist: [null, Validators.required]
    });
  }

  ngOnInit(): void {
    this.errors = [];
  }

  get name(): AbstractControl { return this.addSongForm.get('name'); }
  get album(): AbstractControl { return this.addSongForm.get('album'); }
  get artist(): AbstractControl { return this.addSongForm.get('artist'); }


  addSong(): void {
    const song: Song = {
      name: this.name.value,
      album: this.album.value,
      artist: this.artist.value
    };

    if (this.addSongForm.valid){
      this.songService.addSong(song).subscribe(
        res => this.router.navigate(['/home']).then(),
        err => {
          const songError: SongError = err.error;
          this.errors = songError.error;
        }
      );
    }

  }
}
