import { Component, OnInit } from '@angular/core';
import {Artist} from '../../Model/artist';
import {ArtistService} from '../../Service/artist.service';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.scss']
})
export class ArtistComponent implements OnInit {

  artists: Artist[];

  constructor(private artistService: ArtistService) { }

  ngOnInit(): void {
    this.getArtists();
  }

  getArtists(): void{
    this.artistService.getArtists().subscribe(artists => this.artists = artists);
  }
}
