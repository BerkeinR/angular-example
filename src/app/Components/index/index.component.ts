import { Component, OnInit } from '@angular/core';
import {Song} from '../../Model/song';
import {SongService} from '../../Service/song.service';
import {delay, startWith, switchMap} from 'rxjs/operators';
import {SongError} from '../../Model/song-error';
import {interval} from 'rxjs';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})

export class IndexComponent implements OnInit {

  songs: Song[];
  errors: string[];

  constructor(private songService: SongService) { }

  ngOnInit(): void {
    this.getSongs();
    this.errors = [];
  }

  getSongs(): void {
    interval(5000)
      .pipe(
        startWith(0),
        switchMap(() => this.songService.getSongs())
      )
      .subscribe(songs => {
        if (!(JSON.stringify(this.songs) === JSON.stringify(songs))) {
          this.songs = songs;
        }
      });
  }

  deleteSong(song: Song): void {
    if (song != null){
      const index = this.songs.indexOf(song, 0);
      if (index > -1) {
        this.songs.splice(index, 1);
      }
    }
  }

  deleteSongError(songError: SongError): void {
    if (songError != null){
      this.errors = songError.error;
    }
  }
}
