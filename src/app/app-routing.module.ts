import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {IndexComponent} from './Components/index/index.component';
import {AddSongComponent} from './Components/song/add-song/add-song.component';
import {EditSongComponent} from './Components/song/edit-song/edit-song.component';
import {SearchComponent} from './Components/search/search.component';
import {ArtistComponent} from './Components/artist/artist.component';

const routes: Routes = [
  {path: 'home', component: IndexComponent},
  {path: 'add-song', component: AddSongComponent},
  {path: 'edit-song/:uniqueID', component: EditSongComponent},
  {path: 'search', component: SearchComponent},
  {path: 'artists', component: ArtistComponent},
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  // { path: '**', component: IndexComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
