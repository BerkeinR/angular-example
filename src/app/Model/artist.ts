export class Artist{
  name: string;
  stagename: string;
  genre: string;
  birthdate: number;
}
