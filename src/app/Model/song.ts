export class Song {
  uniqueID?: string;
  name: string;
  artist: string;
  album: string;
}
