import {Song} from './song';

export class SongError {
  error: string[];
  music?: Song;
}
