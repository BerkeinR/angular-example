import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {Artist} from '../Model/artist';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ArtistService {

  private artistUrl = 'http://localhost:8080/Servlet?command=';


  constructor(private http: HttpClient) { }

  getArtists(): Observable<Artist[]> {
    return this.http.get<Artist[]>(this.artistUrl + 'GetNewArtists');
  }
}
