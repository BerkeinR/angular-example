import { Injectable } from '@angular/core';
import {Observable, of, throwError} from 'rxjs';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Song} from '../Model/song';
import {SONGS} from '../Model/song-mock';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SongService {

  private songUrl = 'http://localhost:8080/Servlet?command=';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json'})
  };

  private headerOptions = new HttpHeaders({ 'Content-Type': 'application/json'});


  songs = SONGS;
  constructor(private http: HttpClient) { }

  // getSongs(): Observable<Song[]> {
  //   return of(this.songs);
  // }

  getSongs(): Observable<Song[]> {
    return this.http.get<Song[]>(this.songUrl + 'GetNewSongs');
  }

  getSong(uniqueID: string): Observable<Song> {
    return this.http.get<Song>(this.songUrl + 'GetSong&uniqueID=' + uniqueID);
  }


  addSong(song: Song): Observable<any> {
    const url = this.songUrl + 'AddSongJson';
    const requestBody = JSON.stringify(song);

    return this.http.post(url, requestBody );

  }

  updateSong(song: Song): Observable<any>  {
    const url = this.songUrl + 'UpdateSongJson';
    const requestBody = JSON.stringify(song);
    return this.http.post(url, requestBody );
  }

  deleteSong(song: Song): Observable<any> {
    const url = this.songUrl + 'DeleteMusic&songName=' + song.name;
    return this.http.post(url, this.headerOptions );
  }


  search(term: string): Observable<Song[]> {
    return this.http
      .get<Song[]>(this.songUrl + 'search&searchItem=' + term);
  }
}
